FROM debian:testing

RUN apt-get update
RUN apt-get install -y \
    curl \
    python-pip

RUN curl -L https://github.com/vkobel/ethereum-generate-wallet/raw/master/lib/x86-64/keccak-256sum --output /usr/local/bin/keccak-256sum
RUN chmod +x /usr/local/bin/keccak-256sum

ADD . /tools
RUN chmod +x /tools/generateWallet*
RUN cd /tools && pip install -r requirements.txt
RUN ln -s /tools/generateWallet /usr/local/bin/generateWallet
RUN ln -s /tools/generateWalletBulk /usr/local/bin/generateWalletBulk